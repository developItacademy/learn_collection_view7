//
//  UserDefaulltsHelper.swift
//  LearnCollectionView
//
//  Created by Steven Hertz on 10/3/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import Foundation

enum UserDefaultKeys: String {
    case groupNumber
    static var keyGroupNumber: String {
        return UserDefaultKeys.groupNumber.rawValue
    }
}


class UserDefaulltsHelper {
    
    
    static func writeGroupUserDefaults(withGroupNumber groupNumber: String = "9") {
        UserDefaults.standard.set(groupNumber, forKey: UserDefaultKeys.keyGroupNumber)
    }
    
    static func readGroupUserDefaults() -> String {
        guard let groupNumber = UserDefaults.standard.object(forKey: UserDefaultKeys.keyGroupNumber) as? String else  { fatalError("Error - Getting default value for UserDefault") }
        return groupNumber
    }
    
    static func removeUserDefaults() {
        UserDefaults.standard.removeObject(forKey: UserDefaultKeys.keyGroupNumber)
    }
 
    static func registerUserDefaults() {
        UserDefaults.standard.register(defaults: [ UserDefaultKeys.keyGroupNumber : "9"])
    }
}
