//
//  DataForApp.swift
//  LearnCollectionView
//
//  Created by Steven Hertz on 9/26/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import Foundation

class DataForApp {
    
    var students : [User] = []
    
    var studentUsernames: [String] = []
    var studentData : [String: User]?
    
    static func fromNine() -> DataForApp {
        let dataApp = DataForApp()
        GetStudents.getStudentFromJamfSchool(fromWhichGroupNumber: "9") { students in
            students.forEach { (student) in
                print(student.firstName)
                dataApp.studentUsernames.append(student.username)
            }
            //        dataApp.studentUsernames = ["Sam", "Larry", "Tom"]
        }
        return dataApp
    }
    
//    init(FromWhichGroup groupNumber: String) {
//        var studentUsernamesx : [String] = []
//        var studentDatax : [String: User] = [:]
//            GetStudents.getStudentFromJamfSchool(fromWhichGroupNumber: "9") { students in
//            students.forEach { (student) in
//                print(student.firstName)
//                studentUsernamesx.append(student.username)
//                studentDatax[student.username] = student
//            }
//        }
//        self.studentUsernames = studentUsernamesx
//        self.studentData = studentDatax
//    }
}


