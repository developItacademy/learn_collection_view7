//
//  HeaderReusableView.swift
//  LearnCollectionView
//
//  Created by Steven Hertz on 8/15/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import UIKit

class HeaderReusableView: UICollectionReusableView {
        
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var label: UILabel!
    
}
